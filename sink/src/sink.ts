/* tslint-disable */
import streaming from 'node-nats-streaming';
import redis from 'src/redis';

function updateUser(id: string, payload: string, userDB: any): void {
  userDB = payload;
  userDB.id = id;
  redis.sadd('email', userDB.email);
  redis.set(id, JSON.stringify(userDB));
  redis.set(userDB.email, id);
}

async function setupSink(): Promise<any> {
  const stan: any = await streaming.connect('test-cluster', 'auth-sink', {
    url: 'nats://streaming:4222',
  });

  console.log('🚀 Sink ready');

  stan.on('connect',  () => {
    const opts: any = stan.subscriptionOptions();
    opts.setDeliverAllAvailable();
    const newUserSubscription: any = stan.subscribe('new.user', opts);
    newUserSubscription.on('message', (msg: any) => {
      const userCreated: any = JSON.parse(msg.getData());
      const id: string = userCreated.payload;
      redis.set(id, JSON.stringify( { id } ));

      // ====== Subscribe to User updates =====
      const updateUserSubscription: any = stan.subscribe(`user.${id}`, opts);
      updateUserSubscription.on('message', (userMsg: any) => {
        const { payload }: any = JSON.parse(userMsg.getData());

        redis.get(id, (___: Error, reply: string) => {
          updateUser(id, payload, JSON.parse(reply));
        });
      });
    });
  });
}

setupSink();
