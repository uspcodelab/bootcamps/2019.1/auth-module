import { Context } from 'koa';

import { User } from './types.d';

async function changeName(args: any, context: Context): Promise<User> {
  const userString: string = await context.redis.getAsync(args.id);
  const user: User = JSON.parse(userString);

  // user.name = args.name;

  context.nats.publish(`user_${user.id}`, JSON.stringify(user));

  return user;
}

export default changeName;
