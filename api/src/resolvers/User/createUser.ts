import {Context} from 'koa';
import redisClient from 'src/redis';
import Event from 'src/resolvers/event';
import {User, UserInput, UserInputData} from 'src/resolvers/User/types';
import {v4 as uuid} from 'uuid';

async function createUser(args: UserInput, context: Context): Promise<User> {
  const {email, password, checkPassword}: UserInputData = args.user;

  if (!email) {
    throw new context.ApolloError('Field email must not be blank', '400');
  }
  if (!password) {
    throw new context.ApolloError('Field password must not be blank', '400');
  }
  if (!checkPassword) {
    throw new context.ApolloError(
        'Field checkPassword must not be blank', '400');
  }
  if (checkPassword !== password) {
    throw new context.ApolloError(
        'Fields password and checkPassword must be equal', '400');
  }

  redisClient.sismember('email', (_: any, res: any) => {
    if (res) {
      throw new context.ApolloError(
        'Email already in database', '400');
    }
  });

  const user: User = {
    id: uuid(),
    email,
    password,
  };

  const createdUser: Event = new Event('createdUser', user.id);

  // const updatedUserEmail: Event = new Event('updatedUserEmail', user.email);

  // const updatedUserPassword: Event =
  //     new Event('updatedUserPassword', user.password);
  const updatedUser: Event = new Event('updatedUser', user);

  context.nats.publish(`new.user`, JSON.stringify(createdUser));
  context.nats.publish(`user.${user.id}`, JSON.stringify(updatedUser));
  // context.nats.publish(`user.${user.id}`, JSON.stringify(updatedUserPassword));

  return user;
}

export default createUser;
