import jwt from 'jsonwebtoken';
import { Context } from 'koa';
import redisClient from 'src/redis';
import {
  AuthPayload,
  User,
  UserLogin,
  UserLoginData,
} from 'src/resolvers/User/types';

const APP_SECRET: string = 'cenha';

async function login(args: UserLogin, context: Context): Promise<AuthPayload> {
  const { email, password }: UserLoginData = args.user;
  let token: AuthPayload = { token: '' };

  if (!email) {
    throw new context.ApolloError('Field email must not be blank', '400');
  }
  if (!password) {
    throw new context.ApolloError('Field password must not be blank', '400');
  }

  const isMember: number = await redisClient.sismember('email', email);

  if (!isMember) {
    throw new context.ApolloError('Email not registered', '404');
  }

  let userId: string = '';

  // Fetch user id based on email
  await redisClient.get(`${email}`, async (err: Error, reply: string) => {
    if (err) {
      throw new context.ApolloError('Could not find user', '404');
    }

    userId = reply;
    if (!userId) {
      throw new context.ApolloError('Could not find user', '404');
    }
     // Fetch user data using its id
    await redisClient.get(userId, (err2: Error, reply2: string) => {
      if (err2) {
        throw new context.ApolloError('Could not fetch user data', '404');
      }

      const userData: User = JSON.parse(reply2);

      if (password !== userData.password) {
        throw new context.ApolloError('Wrong password', '401');
      }

      token = buildToken(userId);
    });
  });

  const delay: any = (ms: any): any => new Promise((res: any): any => setTimeout(res, ms));

  await delay(500);

  return token;
}

function buildToken(id: any): AuthPayload {
  const auth: AuthPayload = {
    token: jwt.sign({ userId: id }, APP_SECRET),
  };
  return auth;
}

export default login;
