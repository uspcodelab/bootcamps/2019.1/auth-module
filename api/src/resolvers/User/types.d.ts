import {v4 as uuid} from 'uuid';

export interface User {
  id: UUID;
  email: string;
  password: string;
}

interface UserInputData {
  email: string;
  password: string;
  checkPassword: string;
}

interface UserLoginData {
  email: string;
  password: string;
}

export interface UserLogin {
  user: UserLoginData;
}

export interface AuthPayload {
  token: string;
}

export interface UserInput {
  user: UserInputData;
}

export interface UserQueryById {
  id: string;
}
