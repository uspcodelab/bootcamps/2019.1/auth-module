import { Context } from 'koa';

import { AuthPayload, User, UserInput } from 'src/resolvers/User/types';

// Queries
import getUser from 'src/resolvers/User/getUser';

// Mutations
import changeName from 'src/resolvers/User/changeName';
import createUser from 'src/resolvers/User/createUser';
import login from 'src/resolvers/User/login';

export const UserQuery: any = {
  getUser(_: any, args: any, context: Context): Promise<User> {
    return getUser(args, context);
  },
};

export const UserMutation: any = {
  createUser(_: any, args: UserInput, context: Context): Promise<User> {
    return createUser(args, context);
  },
  changeUserName(_: any, args: any, context: Context): Promise<User> {
    return changeName(args, context);
  },
  login(_: any, args: any, context: Context): Promise<AuthPayload> {
    return login(args, context);
  },
};
