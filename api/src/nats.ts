import stan from 'node-nats-streaming';

async function start(): Promise<any> {
  return await stan.connect('test-cluster', 'auth-api', { url: 'nats://streaming:4222' });
}

export default start();
